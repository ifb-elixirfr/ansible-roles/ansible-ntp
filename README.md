# NTP client configuration

## Manage chronyd

Start chronyd and define ntp servers to use from `ntp_servers_chronyd` or default centos ntp pool servers

If variable `ntp_servers_chronyd` is defined, replace default NTP server in `chronyd` configuration.

Exemple:
```
ntp_servers_chronyd: |
  server 0.pool.ntp.org iburst
  server 1.pool.ntp.org iburst
  server 2.pool.ntp.org iburst
  server 3.pool.ntp.org iburst
```
